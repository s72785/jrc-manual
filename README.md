This is about a Manual for the 
Radical Militant Library

## read and contribute to the manual

... in `[eadingclub-man.md](./eadingclub-man.md)`

read first and then maybe add your own experiences.

## how to build

in case you have `pandoc` you can have it as TeX, HTML, ODT and many more
for PDF you will need `pdflatex` or alike

for TeX, PDF and HTML5 just start:

```sh
$ make
```

or for a single, specific target like ODT:

```sh
$ make odt
```

